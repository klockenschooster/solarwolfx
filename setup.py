#!/usr/bin/env python3
from setuptools import setup
setup(
    name = 'SolarWolfX',
    version = '1.6.0',
    entry_points = {
        'console_scripts': [
            'solarwolfx = solarwolfx:entrypoint',
        ]
    },
    install_requires = [
        'pygame>=1.9.6',
    ],
    packages = [
        'solarwolfx',
    ],
    package_dir={
        'solarwolfx': 'solarwolfx',
    },
    package_data={
        'solarwolfx': [
            'data/*',
            'data/music/*',
            'data/audio/*',
        ],
    },
)
