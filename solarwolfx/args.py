import argparse

__parser = argparse.ArgumentParser(prog="solarwolfx",
                                   description='Arcade/Action space game.')
__parser.add_argument('--nosound', default=False, action='store_true',
                      help='disable all sound and music')
__parser.add_argument('--fullscreen', default=False, action='store_true',
                      help='fullscreen mode (modechange to 800x600)')


__args = __parser.parse_args()


def nosound():
    return __args.nosound


def fullscreen():
    return __args.fullscreen
